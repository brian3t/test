import {IonButton, IonContent, IonHeader, IonPage, IonTitle, IonToggle, IonToolbar} from '@ionic/react';
// import ExploreContainer from '../components/ExploreContainer';
import './Home.css';
import {BiometryType, NativeBiometric} from "capacitor-native-biometric";
// import Swal from "sweetalert2"
import moment from 'moment'
import {useEffect, useState} from "react"

const MWCOG_URL = "https://tdm.commuterconnections.org/mwcog/"

function swl(title, msg, duration = 2000){
  alert(title + ' ' + msg)
  /*Swal.fire({
    title: title,
    html: msg,
    timer: duration,
    didOpen: () => {
      Swal.showLoading()
    },
    willClose: () => {
    }
  }).then((result) => {
    /!* Read more about handling dismissals below *!/
    if (result.dismiss === Swal.DismissReason.timer) {
      console.log('Sweet alert was closed by the timer')
    }
  })*/

}

/**
 * Format build ver to Day Hour:Minute
 * @param build_ver
 */
function build_ver_format(build_ver){
  let bv = moment(build_ver)
  if (!(bv.isValid())) return 'bad date format'
  return bv.format('DD HH:mm')
}

const Home = () => {
  const [build_ver, set_build_ver] = useState('missing_build_ver')
  const [is_checked, set_is_checked] = useState(false)
  const [is_checked_ion, set_is_checked_ion] = useState(true)
  const [is_checked_pure, set_is_checked_pure] = useState(true)

  const change_is_checked = async function(e){
    console.log(`e: `, e)
    console.log(`e checked: `, e.target.checked)
    if (e.target.checked == false){
      console.log(`trying to prevent`)
      e.target.checked = true
      return
    }
  }
  const change_is_checked_ion = async function(e){
    console.log(`e: `, e)
    console.log(`e checked: `, e.detail.checked)
    if (e.detail.checked == false){
      console.log(`trying to prevent`)
      set_is_checked(is_checked => true) //prevent setting to false
    }
  }

  const change_is_checked_pure = async function(e){
    console.log(`pure e: `, e)
    console.log(`e checked: `, e.target.checked)
    if (e.target.checked == false){
      console.log(`trying to prevent`)
      set_is_checked_pure(is_checked => true) //prevent setting to false
      return
    }
    set_is_checked_pure(e.target.checked)
  }

  // @ts-ignore
  async function performBiometricVerification(){
    const result = await NativeBiometric.isAvailable();
    console.warn(`native bio avail: `, result)
    swl('native biometric avail: ', result ? 'True' : 'False')
    if (! result.isAvailable) return;

    //const isFaceID = result.biometryType == BiometryType.FACE_ID;
    console.warn(`Biometric method `, result.biometryType)
    swl(`Biometric method `, result.biometryType)
    const verified = await NativeBiometric.verifyIdentity({
      reason: "Please log in using your fingerprint or face recognition",
      title: "Log in",
      subtitle: "MWCOG",
      description: "",
    })
      .then(() => true)
      .catch(() => false);
    console.warn(`verified `, verified)
    swl('Verification result', verified ? 'Successful' : 'Unsuccessful')

    if (! verified) return;

    /*const credentials = await NativeBiometric.getCredentials({
      server: MWCOG_URL,
    });
    console.warn(`credentials `, credentials)*/
  }

// Save user's credentials
  NativeBiometric.setCredentials({
    username: "username_here",
    password: "password_here",
    server: MWCOG_URL,
  }).then();

// Delete user's credentials
  NativeBiometric.deleteCredentials({
    server: MWCOG_URL,
  }).then();

  async function save_cred(){
    const res = await NativeBiometric.setCredentials({username: 'username_here', password: 'password_here', server: MWCOG_URL})
    swl('save cred res', res?.toString())
  }

  async function load_cred(){
    const credentials = await NativeBiometric.getCredentials({
      server: MWCOG_URL,
    });
    alert(JSON.stringify(credentials))
    swl('load cred res', JSON.stringify(credentials), 20000)
  }

  useEffect(() => {
    async function fetch_data(){
      console.log(`useeffect called`)
      console.log(`build ver loaded:`, window.BUILD_VER)
      set_build_ver(build_ver_format(window.BUILD_VER))
      const bio_res = NativeBiometric.isAvailable()
      alert((await bio_res).biometryType)
    }

    fetch_data()
  })

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>version: {build_ver}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonToggle checked={is_checked} onClick={change_is_checked}></IonToggle>
        {/*<input type="checkbox" checked={is_checked_pure} onChange={change_is_checked_pure} />*/}
        {/*<ion-toggle type="checkbox" checked={is_checked_ion} onClick={change_is_checked_ion}></ion-toggle>*/}
        <IonButton onClick={performBiometricVerification}>
          Save Username Pw
        </IonButton>
        <IonButton onClick={save_cred}>
          Save Cred
        </IonButton>
        <IonButton onClick={load_cred}>
          Load Cred
        </IonButton>
        <div>
          <span>BioMetric Types</span>
          <ol>
            <li>0 - NONE </li>
            <li>1 - TOUCH_ID </li>
            <li>2 - FACE_ID </li>
            <li>3 - FINGERPRINT </li>
            <li>4 - FACE_AUTHENTICATION </li>
            <li>5 - IRIS_AUTHENTICATION </li>
            <li>6 - MULTIPLE </li>
          </ol>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Home;
